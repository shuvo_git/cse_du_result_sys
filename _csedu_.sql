-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2018 at 09:59 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `_csedu_`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `credit` double NOT NULL,
  `semester` int(11) NOT NULL,
  `course_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `credit`, `semester`, `course_type`) VALUES
('CHE-1203', 'Chemistry', 3, 2, 0),
('CSE-1101', 'Fundamentals of Computers and Computing', 2, 1, 0),
('CSE-1102', 'Discrete Mathematics', 3, 1, 0),
('CSE-1111', 'Fundamentals of Computer and Computing Lab', 1.5, 1, 1),
('CSE-1201', 'Fundamentals of Programming', 3, 2, 0),
('CSE-1211', 'Fundamentals of Programming Lab', 3, 2, 1),
('CSE-2101', 'Data Structures and Algorithms', 3, 3, 0),
('CSE-2102', 'Object Oriented Programming', 3, 3, 0),
('CSE-2111', 'Data Structures and Algorithms Lab', 1.5, 3, 1),
('CSE-2112', 'Object Oriented Programming Lab', 1.5, 3, 1),
('EEE-1103', 'Electrical Circuits', 3, 1, 0),
('EEE-1113', 'Electrical Circuits Lab', 1.5, 1, 1),
('EEE-1202', 'Digital Logic Design', 3, 2, 0),
('EEE-1212', 'Digital Logic Design Lab', 1.5, 2, 1),
('EEE-2103', 'Electronic Devices and Circuits', 3, 3, 0),
('EEE-2113', 'Electronic Devices and Circuits Lab ', 0.75, 3, 1),
('ENG-1215', 'Developing English Language Skill Lab', 1.5, 2, 1),
('GED-2104', 'Bangladesh Studies', 2, 3, 0),
('MATH-1105', 'Differential and Integral Calculus', 3, 1, 0),
('MATH-1204', 'Method of Integration, Differential Equations, and', 3, 2, 0),
('MATH-2105', 'Linear Algebra', 3, 3, 0),
('PHY-1104', 'Physics', 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `reg_no` char(12) NOT NULL,
  `semester` int(11) NOT NULL,
  `course_id` char(10) NOT NULL,
  `mark` double NOT NULL,
  `appeared_time` int(11) NOT NULL,
  `exam_roll` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`reg_no`, `semester`, `course_id`, `mark`, `appeared_time`, `exam_roll`) VALUES
('2011-412-063', 1, 'CSE-1101', 72, 1, 279),
('2011-412-063', 1, 'CSE-1102', 72, 1, 279),
('2011-412-063', 1, 'CSE-1111', 72, 1, 279),
('2011-412-063', 1, 'EEE-1103', 72, 1, 279),
('2011-412-063', 1, 'EEE-1113', 72, 1, 279),
('2011-412-063', 1, 'MATH-1105', 72, 1, 279),
('2011-412-063', 1, 'PHY-1104', 72, 1, 279),
('2011-412-063', 2, 'CSE-1201', 72, 1, 279),
('2011-412-063', 2, 'EEE-1202', 72, 1, 279),
('2011-412-063', 2, 'MATH-1204', 72, 1, 279),
('2014-412-033', 1, 'CSE-1101', 74, 1, 183),
('2014-412-033', 1, 'CSE-1102', 74, 1, 183),
('2014-412-033', 1, 'CSE-1111', 74, 1, 183),
('2014-412-033', 1, 'EEE-1103', 74, 1, 183),
('2014-412-033', 1, 'EEE-1113', 74, 1, 183),
('2014-412-033', 1, 'MATH-1105', 74, 1, 183),
('2014-412-033', 1, 'PHY-1104', 74, 1, 183),
('2014-412-033', 2, 'CSE-1201', 74, 1, 183),
('2014-412-033', 2, 'EEE-1202', 74, 1, 183),
('2014-412-033', 2, 'MATH-1204', 74, 1, 183),
('2014-412-035', 1, 'CSE-1101', 69, 1, 255),
('2014-412-035', 1, 'CSE-1102', 69, 1, 255),
('2014-412-035', 1, 'CSE-1111', 69, 1, 255),
('2014-412-035', 1, 'EEE-1103', 69, 1, 255),
('2014-412-035', 1, 'EEE-1113', 69, 1, 255),
('2014-412-035', 1, 'MATH-1105', 69, 1, 255),
('2014-412-035', 1, 'PHY-1104', 69, 1, 255),
('2014-412-035', 2, 'CSE-1201', 69, 1, 255),
('2014-412-035', 2, 'EEE-1202', 69, 1, 255),
('2014-412-035', 2, 'MATH-1204', 69, 1, 255);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `email` varchar(200) NOT NULL,
  `password` varchar(200) DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`email`, `password`, `role`) VALUES
('abc@gmail.com', '123456', 2),
('shuvo.pma@gmail.com', '123456', 1);

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE `student_info` (
  `email` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `reg_no` char(12) NOT NULL,
  `roll_no` int(11) NOT NULL,
  `semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`email`, `name`, `reg_no`, `roll_no`, `semester`) VALUES
('abc@gmail.com', 'abc', '0', 0, 0),
('shuvo.pma@gmail.com', 'Jobayed Ullah', '2011-412-063', 107, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`reg_no`,`semester`,`course_id`,`appeared_time`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
  ADD PRIMARY KEY (`email`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `marks`
--
ALTER TABLE `marks`
  ADD CONSTRAINT `marks_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`);

--
-- Constraints for table `student_info`
--
ALTER TABLE `student_info`
  ADD CONSTRAINT `student_info_ibfk_1` FOREIGN KEY (`email`) REFERENCES `student` (`email`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
