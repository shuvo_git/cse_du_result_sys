<?php
	include('Authenticate.php'); // Includes Login Script

	if(isset($_SESSION['login_user']))
	{
		header("location: Home.php");
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo "login"; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
		body
		{
			padding-top:15%;
			background: #FAFAFA;
		} 
		
		#box-panel-login:hover
		{
			box-shadow: 0 0 10px rgba(0,0,0,0.6);
			-moz-box-shadow: 0 0 10px rgba(0,0,0,0.6);
			-webkit-box-shadow: 0 0 10px rgba(0,0,0,0.6);
			-o-box-shadow: 0 0 10px rgba(0,0,0,0.6);
		}	
	</style>
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
            else $('head > link').filter(':first').replaceWith(defaultCSS); 
        }
        
    </script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4" >
				<div class="panel panel-default" id="box-panel-login">
					<div class="panel-heading">
						<h3 class="panel-title">Please sign in</h3>
					</div>
					<div class="panel-body">
						<form action = "" method="POST" accept-charset="UTF-8" role="form" >
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder="E-mail" name="email" type="text" value="">
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Password" name="password" type="password" value="">
								</div>
								<!--div class="checkbox">
									<label>
										<input name="remember" type="checkbox" value="Remember Me"> Remember Me
									</label>
									<label id="form_submit"> new? register account</label>
								</div-->
								<div class="form-group">
									<label>
										<p id="err_fld"></p>
									</label>
								</div>
								<input class="btn btn-sm btn-primary" name="submit" type="submit" value="Login">
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style type="text/css">

		#form_submit
		{
			color: #245db7;
		}
		#form_submit:hover
		{
			color: #4a85e2;
		}

		p #err_fld
		{
			color: #ffff00;
			size:10px;
		}

	</style>

	
	<!--script type="text/javascript">
		$( document ).ready(function() {
          var iframe_height = parseInt($('html').height()); 
          window.parent.postMessage( iframe_height, 'http://bootsnipp.com');






          	$("#form_submit").click(function() {
	            event.preventDefault();

	            var formData = {
	                'email'       :  $('input[name=email]').val(),
	                'password'    :  $('input[name=password]').val() 
	            };
	            console.log(formData);

	            $.ajax({
	                url: "<?php echo site_url('home/register') ?>",
	                type: "POST",
	                data: formData,
	                success: function(d) 
	                {
	                    //alert(d);
	                    $("#err_fld").html("New user Added. login now ...");
	                }
	            });
	        });
        });
	</script-->
	
	
</body>
</html>
