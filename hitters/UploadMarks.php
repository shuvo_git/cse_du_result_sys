
<!-- https://stackoverflow.com/questions/8238407/how-to-parse-excel-file-in-javascript-html5?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa -->



<html>
	<Head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.4/xlsx.core.min.js"></script>
	</head>

	<body>
		<script>
			/* set up XMLHttpRequest */


			// replace it with your file path in local server
			var url = "http://localhost/PROJECT3/marksheet/"+<?php echo $_POST['sheet_name']; ?>;

			var oReq = new XMLHttpRequest();
			oReq.open("GET", url, true);
			oReq.responseType = "arraybuffer";

			oReq.onload = function(e) 
			{
				var arraybuffer = oReq.response;

				/* convert data to binary string */
				var data = new Uint8Array(arraybuffer);

				var arr = new Array();
				for (var i = 0; i != data.length; ++i) {
					arr[i] = String.fromCharCode(data[i]);
				}

				var bstr = arr.join("");

				var cfb = XLSX.read(bstr, { type: 'binary' });
				var dta=[];
				var i=0;

				cfb.SheetNames.forEach(function(sheetName, index) 
				{

					// Obtain The Current Row As CSV
					var fieldsObjs = XLS.utils.sheet_to_json(cfb.Sheets[sheetName]);
					
					
					fieldsObjs.map(function(field) 
					{
						//console.log(field);
						dta[i] = field;
						i++;
						
					});

				});
				
				//console.log(dta);
				
				var formData = {
					'data'       :  dta,
					'semester_no':  <?php echo $_POST['semester_no']; ?>,
					'course_id':  <?php echo $_POST['course_id']; ?>
				};
				$.ajax({
					url: "InsertMarks.php",
					type: "POST",
					data: formData,
					success: function() 
					{
						
						//console.log("success");
					}
				});
								
								
				
				
			}

			oReq.send();
		</script>
		
		
			
		
		
		<div id="my_file_output"></div>
	</body>


</html>
