<?php
	//include('session.php');
	session_start();
	if(!isset($_SESSION['login_user']))
	{
		$log = "not logged";
		header('Location: index.php'); // Redirecting To Home Page
	}
	else $log = $_SESSION['login_user'];
?>


<!DOCTYPE html><!--[if IE 8]><html class="ie8" lang="en"><![endif]--><!--[if IE 9]><html class="ie9" lang="en"><![endif]--><!--[if !IE]><!-->
<html lang="en"><!--<![endif]-->
	<head>
		<title>RESULT - CSEDU</title><!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimum-scale=1,maximum-scale=1">
		<meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="ClipTwo - Responsive Admin Template build with Bootstrap" name="description">
		<meta content="ClipTheme" name="author">
		<link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png">
		<link rel="icon" type="image/ico" sizes="16x16" href="assets/images/favicon9.ico">
		<link rel="manifest" href="assets/images/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="assets/images/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,400italic,600,700|Raleway:100,300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="assets/css/vendors.bundle.min.css"><link rel="stylesheet" href="assets/css/theme.bundle.min.css">
		<link rel="stylesheet" href="assets/css/themes/theme-1.min.css" id="skin_color">
		<link rel="stylesheet" href="assets/css/pe-icon-7-stroke.css" id="skin_color">
		
		
		
		<link rel="stylesheet" href="assets/js/dz/dropzone.css">
		
		
		
		<script type="text/javascript" src="assets/js/dz/dropzone.js"></script>
		
		<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script-->
		<script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.4/xlsx.core.min.js"></script>
		
		
		<style>

			/** Custom Select **/
			.custom-select-wrapper 
			{
				position: relative;
				display: inline-block;
				user-select: none;
			}
			.custom-select-wrapper select 
			{
				display: none;
			}
			.-custom-select 
			{
				position: relative;
				display: inline-block;
			}
			.custom-select-trigger 
			{
				position: relative;
				display: block;
				width: 600px;
				padding: 0 84px 0 22px;
				font-size: 22px;
				font-weight: 200;
				color: #fff;
				line-height: 60px;
				background: #5c9cd8;
				border-radius: 4px;
				cursor: pointer;
			}
			.custom-select-trigger:after 
			{
				position: absolute;
				display: block;
				content: '';
				width: 10px; height: 10px;
				top: 50%; right: 25px;
				margin-top: -3px;
				border-bottom: 1px solid #fff;
				border-right: 1px solid #fff;
				transform: rotate(45deg) translateY(-50%);
				transition: all .4s ease-in-out;
				transform-origin: 50% 0;
			}
			.-custom-select.opened .custom-select-trigger:after 
			{
				margin-top: 3px;
				transform: rotate(-135deg) translateY(-50%);
			}
		  .custom-options {
			position: absolute;
			display: block;
			top: 100%; left: 0; right: 0;
			min-width: 100%;
			margin: 15px 0;
			border: 1px solid #b5b5b5;
			border-radius: 4px;
			box-sizing: border-box;
			box-shadow: 0 2px 1px rgba(0,0,0,.07);
			background: #fff;
			transition: all .4s ease-in-out;
			
			opacity: 0;
			visibility: hidden;
			pointer-events: none;
			transform: translateY(-15px);
		  }
		  .-custom-select.opened .custom-options {
			opacity: 1;
			visibility: visible;
			pointer-events: all;
			transform: translateY(0);
			
			z-index: 2;
		  }
			.custom-options:before {
			  position: absolute;
			  display: block;
			  content: '';
			  bottom: 100%; right: 25px;
			  width: 7px; height: 7px;
			  margin-bottom: -4px;
			  border-top: 1px solid #b5b5b5;
			  border-left: 1px solid #b5b5b5;
			  background: #fff;
			  transform: rotate(45deg);
			  transition: all .4s ease-in-out;
			  
			  z-index: 2;
			}
			.option-hover:before {
			  background: #f9f9f9;
			  z-index: 2;
			}
			.custom-option {
			  position: relative;
			  display: block;
			  padding: 0 22px;
			  border-bottom: 1px solid #b5b5b5;
			  font-size: 18px;
			  font-weight: 600;
			  color: #b5b5b5;
			  line-height: 47px;
			  cursor: pointer;
			  transition: all .4s ease-in-out;
			  z-index: 2;
			}
			.custom-option:first-of-type {
			  border-radius: 4px 4px 0 0;
			  
			  z-index: 2;
			}
			.custom-option:last-of-type {
			  border-bottom: 0;
			  border-radius: 0 0 4px 4px;
			  z-index: 2;
			}
			.custom-option:hover,
			.custom-option.selection {
			  background: #f9f9f9;
			  z-index: 2;
			}
		</style>
	</head>
	<body>
		<div id="app">
		
			<!-- LEFT Navigation Bar Started -->
			<div class="sidebar app-aside" id="sidebar">
				<div class="sidebar-container perfect-scrollbar">
					<nav>
						
						<ul class="main-navigation-menu">
							<li id="nav_home">
								<a href="#" >
									<div class="item-content">
										<div class="item-media">
											<i class="ti-home"></i>
										</div>
										<div class="item-inner">
											<span class="title">Home</span>
											<i class="icon-arrow"></i>
										</div>
									</div>
								</a>
							</li>
							
							
							<?php 
							
							if($_SESSION['data']['role']==1)
							{ ?>
							
							<li id="nav_profile">
								<a href="#">
									<div class="item-content">
										<div class="item-media">
											<i class="ti-user"></i>
										</div>
										<div class="item-inner">
											<span class="title">Profile </span>
											<i class="icon-arrow"></i>
										</div>
									</div>
								</a>
								
							</li>
							
											
							
							<li id="nav_courses">
								<a href="#">
									<div class="item-content">
										<div class="item-media">
											<i class="ti-book"></i>
										</div>
										<div class="item-inner">
											<span class="title">Courses</span>
											<i class="icon-arrow"></i>
										</div>
									</div>
								</a>
							</li>
							
							<li id="nav_result">
								<a href="#">
									<div class="item-content">
										<div class="item-media">
											<i class="ti-bar-chart"></i>
										</div>
										<div class="item-inner">
											<span class="title">Result</span>
											<i class="icon-arrow"></i>
										</div>
									</div>
								</a>
							</li>
							
							<?php }
							
							else
							{ ?>
							
							<li id="nav_upload">
								<a href="#">
									<div class="item-content">
										<div class="item-media">
											<i class="ti-export"></i>
										</div>
										<div class="item-inner">
											<span class="title">Upload </span>
											<i class="icon-arrow"></i>
										</div>
									</div>
								</a>
								
							</li>
							<?php } ?>
							
							<li>
								<a href="Logout.php">
									<div class="item-content">
										<div class="item-media">
											<i class="ti-lock"></i>
										</div>
										<div class="item-inner">
											<span class="title">Logout </span>
											<i class="icon-arrow"></i>
										</div>
									</div>
								</a>
								
							</li>
							
							
						</ul>
						
					</nav>
				</div>
			</div>
			<!-- LEFT Navigation Bar Ends -->
			
			
			<div class="app-content">
			
			
				<!-- HEADER BAR Started -->
				<header class="navbar navbar-expand-lg navbar-light navbar-static-top">
				
					<div class="navbar-header">
						<a href="#" class="sidebar-mobile-toggler float-left d-lg-none" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
							<i class="ti-align-justify"></i> 
						</a>
						<a class="navbar-brand" href="index.html"><img src="assets/images/csedu_logo.png" alt="ClipTwo"> </a>
						
						<a href="#" class="sidebar-toggler float-right d-none d-lg-block d-xl-block" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
							<i class="ti-align-justify"></i>
						</a>
					</div>
					
					
					<div class="container-navbar-right">
						<ul class="navbar-right">
							<!--li class="dropdown">
								<a href="javascript:void(0)" data-parent-tab="#offTab" data-tab="#off-users" class="dropdown-toggle" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar">
									<span class="dot-badge partition-red animated infinite pulse"></span> 
									<i class="ti-comment animated bounce"></i> 
									<span class="d-none d-md-block d-lg-block d-xl-block">MESSAGES</span>
								</a>
							</li-->
							<li class="dropdown">
								<a href class="dropdown-toggle" data-parent-tab="#offTab" data-tab="#off-activities" class="dropdown-toggle" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar">
									<i class="ti-check-box"></i> 
									<span class="d-none d-md-block d-lg-block d-xl-block">ACTIVITIES</span>
								</a>
							</li>
							<li class="dropdown">
								<a href class="dropdown-toggle" data-toggle="dropdown">
									<i class="ti-world"></i> 
									<span class="d-none d-md-block d-lg-block d-xl-block">English</span>
								</a>
								<div role="menu" class="dropdown-menu dropdown-menu-right dropdown-light">
									<a href="#" class="dropdown-item">Bangla </a>
									<a href="#" class="dropdown-item">English </a>
									<a href="#" class="dropdown-item">Italiano</a>
								</div>
							</li>
							
							<li class="dropdown current-user d-none d-md-block d-lg-block d-xl-block">
								<a href class="dropdown-toggle" data-toggle="dropdown">
									<img src="assets/images/user-avatar.png" alt="Peter"> 
										<span class="username"><?php echo $log; ?><i class="ti-angle-down"></i></span>
								</a>
								<ul class="dropdown-menu dropdown-menu-right dropdown-dark">
									<li><a href="pages_user_profile.html">My Profile</a></li>
									<!--li><a href="pages_calendar.html">My Calendar</a></li>
									<li><a hef="pages_messages.html">My Messages (3)</a></li>
									<li><a href="login_lockscreen.html">Lock Screen</a></li-->
									<li><a href="Logout.php">Log Out</a></li>
								</ul>
							</li>
							
						</ul>
						
						
					</div>
					
					<a class="dropdown-off-sidebar sidebar-mobile-toggler d-md-block d-lg-block" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar"></a>
					&nbsp; 
					<a class="dropdown-off-sidebar d-none d-sm-block" data-toggle-class="app-offsidebar-open" data-toggle-target="#app" data-toggle-click-outside="#off-sidebar">&nbsp;</a>
				</header>
				<!-- HEADER BAR Ends -->
				
				<!-- MAIN CONTENT Started -->
				<div class="main-content">
					<div class="wrap-content container" id="container">
						<div class="container-fluid container-fullw bg-white">
						
							<div class="row" id = "homeContent">
							</div>
							
						</div>
					</div>
				</div>
				<!-- MAIN CONTENT Ends -->
			</div>
			
			<!-- Main footer Started -->
			<footer>
				<div class="footer-inner">
					<div class="pull-left">
						&copy; 
						<span class="current-year"></span> 
						<span class="text-bold text-uppercase">TEAM CSEDU</span>. 
						<span>All rights reserved</span>
					</div>
					<div class="pull-right"><span class="go-top"><i class="ti-angle-up"></i></span></div>
				</div>
			</footer>
			<!-- Main footer ends -->
	
			<div id="off-sidebar" class="sidebar">
				<div class="sidebar-wrapper">
					<ul id="offTab" class="nav nav-tabs nav-justified">
						<li class="nav-item"><a href="#off-users" class="nav-link active" aria-controls="off-users" role="tab" data-toggle="tab"><i class="ti-comments"></i></a></li>
						<li class="nav-item"><a href="#off-activities" class="nav-link" aria-controls="off-activities" role="tab" data-toggle="tab"><i class="ti-check-box"></i></a></li>
						<li class="nav-item"><a href="#off-favorites" class="nav-link" aria-controls="off-favorites" role="tab" data-toggle="tab"><i class="ti-heart"></i></a></li>
					</ul>
					
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="off-users">
							<div id="users" toggleable active-class="chat-open">
								<div class="users-list">
									<div class="sidebar-content perfect-scrollbar">
										<h5 class="sidebar-title">On-line</h5>
										<ul class="list-unstyled media-list">
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><i class="fa fa-circle status-online"></i> <img alt="..." src="assets/images/avatar-2.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Nicole Bell</h4><span>Content Designer</span></div></a></li>
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><div class="user-label"><span class="badge badge-success">3</span></div><i class="fa fa-circle status-online"></i> <img alt="..." src="assets/images/avatar-3.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Steven Thompson</h4><span>Visual Designer</span></div></a></li>
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><i class="fa fa-circle status-online"></i> <img alt="..." src="assets/images/avatar-4.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Ella Patterson</h4><span>Web Editor</span></div></a></li>
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><i class="fa fa-circle status-online"></i> <img alt="..." src="assets/images/avatar-5.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Kenneth Ross</h4><span>Senior Designer</span></div></a></li>
										</ul>
										<h5 class="sidebar-title">Off-line</h5>
										<ul class="list-unstyled media-list">
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><img alt="..." src="assets/images/avatar-6.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Nicole Bell</h4><span>Content Designer</span></div></a></li>
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><div class="user-label"><span class="badge badge-success">3</span></div><img alt="..." src="assets/images/avatar-7.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Steven Thompson</h4><span>Visual Designer</span></div></a></li>
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><img alt="..." src="assets/images/avatar-8.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Ella Patterson</h4><span>Web Editor</span></div></a></li>
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><img alt="..." src="assets/images/avatar-9.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Kenneth Ross</h4><span>Senior Designer</span></div></a></li>
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><img alt="..." src="assets/images/avatar-10.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Ella Patterson</h4><span>Web Editor</span></div></a></li>
											<li class="media"><a data-toggle-class="chat-open" data-toggle-target="#users" href="#"><img alt="..." src="assets/images/avatar-5.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Kenneth Ross</h4><span>Senior Designer</span></div></a></li>
										</ul>
									</div>
								</div>
								
								<div class="user-chat">
									<div class="chat-content">
										<div class="sidebar-content perfect-scrollbar">
											<a class="sidebar-back pull-left" href="#" data-toggle-class="chat-open" data-toggle-target="#users">
												<i class="ti-angle-left"></i> 
												<span>Back</span>
											</a>
											<ol class="discussion">
												<li class="messages-date">Sunday, Feb 9, 12:58</li>
												<li class="self"><div class="message"><div class="message-name">Peter Clark</div><div class="message-text">Hi, Nicole</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">How are you?</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div></li>
												<li class="other"><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">Hi, i am good</div><div class="message-avatar"><img src="assets/images/avatar-2.jpg" alt=""></div></div></li>
												<li class="self"><div class="message"><div class="message-name">Peter Clark</div><div class="message-text">Glad to see you ;)</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div></li>
												<li class="messages-date">Sunday, Feb 9, 13:10</li>
												<li class="other"><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">What do you think about my new Dashboard?</div><div class="message-avatar"><img src="assets/images/avatar-2.jpg" alt=""></div></div></li>
												<li class="messages-date">Sunday, Feb 9, 15:28</li>
												<li class="other"><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">Alo...</div><div class="message-avatar"><img src="assets/images/avatar-2.jpg" alt=""></div></div><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">Are you there?</div><div class="message-avatar"><img src="assets/images/avatar-2.jpg" alt=""></div></div></li>
												<li class="self"><div class="message"><div class="message-name">Peter Clark</div><div class="message-text">Hi, i am here</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">Your Dashboard is great</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div></li>
												<li class="messages-date">Friday, Feb 7, 23:39</li>
												<li class="other"><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">How does the binding and digesting work in AngularJS?, Peter?</div><div class="message-avatar"><img src="assets/images/avatar-2.jpg" alt=""></div></div></li>
												<li class="self"><div class="message"><div class="message-name">Peter Clark</div><div class="message-text">oh that's your question?</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div><div class="message"><div class="message-name">Peter Clark</div><div class="message-text">little reduntant, no?</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div><div class="message"><div class="message-name">Peter Clark</div><div class="message-text">literally we get the question daily</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div></li>
												<li class="other"><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">I know. I, however, am not a nerd, and want to know</div><div class="message-avatar"><img src="assets/images/avatar-2.jpg" alt=""></div></div></li>
												<li class="self"><div class="message"><div class="message-name">Peter Clark</div><div class="message-text">for this type of question, wouldn't it be better to try Google?</div><div class="message-avatar"><img src="assets/images/avatar-1.jpg" alt=""></div></div></li>
												<li class="other"><div class="message"><div class="message-name">Nicole Bell</div><div class="message-text">Lucky for us :)</div><div class="message-avatar"><img src="assets/images/avatar-2.jpg" alt=""></div></div></li>
											</ol>
										</div>
									</div>
									<div class="message-bar">
										<div class="message-inner">
											<a class="link icon-only" href="#"><i class="fa fa-camera"></i></a>
											<div class="message-area"><textarea placeholder="Message"></textarea></div>
											<a class="link" href="#">Send</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div role="tabpanel" class="tab-pane" id="off-activities">
							<div class="sidebar-content perfect-scrollbar">
								<h5 class="sidebar-title">Activities</h5>
								<div class="list-group no-margin"><a class="media list-group-item list-group-item-action text-center" href=""><img class="rounded-circle" alt="Avatar" src="assets/images/avatar-1.jpg"> <span class="media-body block no-margin">Use awesome animate.css <small class="block text-grey">10 minutes ago</small> </span></a><a class="media list-group-item list-group-item-action text-center" href=""><span class="media-body block no-margin">1.0 initial released <small class="block text-grey">1 hour ago</small> </span></a><a class="media list-group-item list-group-item-action text-center" href=""><span class="media-body block no-margin">Briefing project <small class="block text-grey">2 hour ago</small> </span></a><a class="media list-group-item list-group-item-action text-center" href=""><span class="media-body block no-margin">Update jQuery <small class="block text-grey">3 hour ago</small></span></a></div>
							</div>
						</div>
						
						<div role="tabpanel" class="tab-pane" id="off-favorites">
							<div class="users-list">
								<div class="sidebar-content perfect-scrollbar">
									<h5 class="sidebar-title">Favorites</h5>
									<ul class="list-unstyled media-list">
										<li class="media"><a href="#"><img alt="..." src="assets/images/avatar-7.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Nicole Bell</h4><span>Content Designer</span></div></a></li>
										<li class="media"><a href="#"><div class="user-label"><span class="badge badge-success">3</span></div><img alt="..." src="assets/images/avatar-6.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Steven Thompson</h4><span>Visual Designer</span></div></a></li>
										<li class="media"><a href="#"><img alt="..." src="assets/images/avatar-10.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Ella Patterson</h4><span>Web Editor</span></div></a></li>
										<li class="media"><a href="#"><img alt="..." src="assets/images/avatar-2.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Kenneth Ross</h4><span>Senior Designer</span></div></a></li>
										<li class="media"><a href="#"><img alt="..." src="assets/images/avatar-4.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Ella Patterson</h4><span>Web Editor</span></div></a></li>
										<li class="media"><a href="#"><img alt="..." src="assets/images/avatar-5.jpg" class="media-object"><div class="media-body"><h4 class="media-heading">Kenneth Ross</h4><span>Senior Designer</span></div></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	
			
			
		</div>
		
		
		
		<script src="assets/js/vendors.bundle.min.js"></script>
		<script src="vendor/Chart.min.js"></script>
		<script src="vendor/jquery.sparkline/jquery.sparkline.min.js"></script>
		
		<script src="assets/js/main.min.js"></script>
		
		<script src="vendor/dropzone/dropzone.js"></script>
		
		
		
		
		<!--script src="vendor/bootstrap/js/bootstrap.min.js"></script-->
		<script>
			NProgress.configure({showSpinner:!1}),NProgress.start(),NProgress.set(.4);var interval=setInterval(function(){NProgress.inc()},1e3);jQuery(document).ready(function(){NProgress.done(),clearInterval(interval),Main.init()})
		</script>
		
		<script src="assets/js/index.min.js"></script>
		<script type="text/javascript" >
		
			function markToGPA(mark)
			{
				if(mark>79)return 4.00;
				if(mark>74)return 3.75;
				if(mark>69)return 3.50;
				if(mark>64)return 3.25;
				if(mark>59)return 3.00;
				if(mark>54)return 2.75;
				if(mark>49)return 2.50;
				if(mark>44)return 2.25;
				if(mark>39)return 2.00;
				
				return 0.00;
			}
			function loadHomeContent()
			{
			
				
					var title       = ["Result Generate", "Notify", "Manage Backend"];
					var description = ["...", "...", "..."];
					var icon        = ["ti-bar-chart","ti-bell","ti-server"];
					var str         = '';
					
					for(var i=0;i<3;i++)
					{
				
						str += 
						'<div class="col-sm-4">'+
							'<div class="panel panel-white no-radius text-center">' +
								'<div class="panel-body">' +
									'<span class="fa-stack fa-2x">' +
										
										'<i class="'+icon[i]+'"></i>' +
										
									'</span>' +
									'<h2 class="StepTitle">'+title[i]+'</h2>' +
									'<p class="text-small">'+description[i]+'</p> ' +
									'<p class="links cl-effect-1"><a href="">view more</a></p>' +
								'</div>' +
							'</div>' +
						'</div>';
					}
					
					return str;
					//this.empty().append(str);
			}
			
			function loadProfileContent()
			{
				var str='';
					str +=	'<div class="col-md-12">'+
							'	<div class="tabbable">'+
							'		<ul class="nav nav-tabs" id="myTab4">'+
							'			<li class="nav-item padding-top-5 padding-left-5"><a href="#panel_overview" class="nav-link active" data-toggle="tab">Overview</a></li>'+
							'			<li class="nav-item padding-top-5"><a href="#panel_edit_account" class="nav-link" data-toggle="tab">Edit Account</a></li>'+
							'		</ul>'+
									
							'		<div class="tab-content">'+
							'			<div id="panel_overview" class="tab-pane active">'+
							'				<div class="row">'+
												
							'					<div class="col-sm-7 col-md-8"><div class="user-left"><div class="center"><h4>'+'<?php echo $log; ?>'+'</h4><div class="fileinput fileinput-new" data-provides="fileinput"><div class="user-image"><div class="fileinput-new img-thumbnail"><img src="assets/images/user-avatar.png" alt=""></div><div class="fileinput-preview fileinput-exists img-thumbnail"></div><div class="user-image-buttons"><span class="btn btn-azure btn-file btn-sm"><span class="fileinput-new"><i class="fa fa-pencil"></i> </span><span class="fileinput-exists"><i class="fa fa-pencil"></i> </span><input type="file"> </span><a href="#" class="btn fileinput-exists btn-red btn-sm" data-dismiss="fileinput"><i class="fa fa-times"></i></a></div></div></div><hr><div class="social-icons block"><ul><li data-placement="top" data-original-title="Twitter" class="social-twitter tooltips"><a href="http://www.twitter.com" target="_blank">Twitter</a></li><li data-placement="top" data-original-title="Facebook" class="social-facebook tooltips"><a href="http://facebook.com" target="_blank">Facebook</a></li><li data-placement="top" data-original-title="Google" class="social-google tooltips"><a href="http://google.com" target="_blank">Google+</a></li><li data-placement="top" data-original-title="LinkedIn" class="social-linkedin tooltips"><a href="http://linkedin.com" target="_blank">LinkedIn</a></li><li data-placement="top" data-original-title="Github" class="social-github tooltips"><a href="#" target="_blank">Github</a></li></ul></div><hr></div><table class="table table-condensed"><thead><tr><th colspan="3">Contact Information</th></tr></thead><tbody><tr><td>url</td><td><a href="#">www.example.com</a></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>email:</td><td><a href="">peter@example.com</a></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>phone:</td><td>(641)-734-4763</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>skye</td><td><a href="">peterclark82</a></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr></tbody></table><table class="table"><thead><tr><th colspan="3">General information</th></tr></thead><tbody><tr><td>Position</td><td>UI Designer</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Last Logged In</td><td>56 min</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Position</td><td>Senior Marketing Manager</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Supervisor</td><td><a href="#">Kenneth Ross</a></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Status</td><td><span class="badge badge-sm badge-info">Administrator</span></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr></tbody></table><table class="table"><thead><tr><th colspan="3">Additional information</th></tr></thead><tbody><tr><td>Birth</td><td>21 October 1982</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Groups</td><td>New company web site development, HR Management</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr></tbody></table></div></div>'+
												
							'				</div>'+
							'			</div>'+
										
							'			<div id="panel_edit_account" class="tab-pane">'+
							'				<form action="#" role="form" id="form">'+
							'					<fieldset><legend>Account Info</legend><div class="row"><div class="col-md-6"><div class="form-group"><badge class="control-badge">First Name</badge><input type="text" placeholder="Peter" class="form-control" id="firstname" name="firstname"></div><div class="form-group"><badge class="control-badge">Last Name</badge><input type="text" placeholder="Clark" class="form-control" id="lastname" name="lastname"></div><div class="form-group"><badge class="control-badge">Email Address</badge><input type="email" placeholder="peter@example.com" class="form-control" id="email" name="email"></div><div class="form-group"><badge class="control-badge">Phone</badge><input type="email" placeholder="(641)-734-4763" class="form-control" id="phone" name="email"></div><div class="form-group"><badge class="control-badge">Password</badge><input type="password" placeholder="password" class="form-control" name="password" id="password"></div><div class="form-group"><badge class="control-badge">Confirm Password</badge><input type="password" placeholder="password" class="form-control" id="password_again" name="password_again"></div></div><div class="col-md-6"><div class="form-group"><badge class="control-badge">Gender</badge><div class="clip-radio radio-primary"><input type="radio" value="female" name="gender" id="us-female"><badge for="us-female">Female</badge><input type="radio" value="male" name="gender" id="us-male" checked="checked"><badge for="us-male">Male</badge></div></div><div class="row"><div class="col-md-4"><div class="form-group"><badge class="control-badge">Zip Code</badge><input class="form-control" placeholder="12345" type="text" name="zipcode" id="zipcode"></div></div><div class="col-md-8"><div class="form-group"><badge class="control-badge">City</badge><input class="form-control tooltips" placeholder="London (UK)" type="text" data-original-title="We\'ll display it when you write reviews" data-rel="tooltip" title="" data-placement="top" name="city" id="city"></div></div></div><div class="form-group"><badge>Image Upload</badge><div class="fileinput fileinput-new mt-3" data-provides="fileinput"><div class="user-image"><div class="fileinput-new img-thumbnail"><img src="assets/images/avatar-1-xl.jpg" alt=""></div><div class="fileinput-preview fileinput-exists img-thumbnail"></div><div class="user-image-buttons"><span class="btn btn-azure btn-file btn-sm"><span class="fileinput-new"><i class="fa fa-pencil"></i> </span><span class="fileinput-exists"><i class="fa fa-pencil"></i> </span><input type="file"> </span><a href="#" class="btn fileinput-exists btn-red btn-sm" data-dismiss="fileinput"><i class="fa fa-times"></i></a></div></div></div></div></div></div></fieldset>'+
							'					<fieldset><legend>Additional Info</legend><div class="row"><div class="col-md-6"><div class="form-group"><badge class="control-badge">Twitter</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-twitter"></i></span></div><div class="form-group"><badge class="control-badge">Facebook</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-facebook"></i></span></div><div class="form-group"><badge class="control-badge">Google Plus</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-google-plus"></i></span></div></div><div class="col-md-6"><div class="form-group"><badge class="control-badge">Github</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-github"></i></span></div><div class="form-group"><badge class="control-badge">Linkedin</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-linkedin"></i></span></div><div class="form-group"><badge class="control-badge">Skype</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-skype"></i></span></div></div></div></fieldset>'+
											
							'					<div class="row"><div class="col-md-12"><div>Required Fields<hr></div></div></div>'+
												
							'					<div class="row">'+
							'						<div class="col-md-8"><p>By clicking UPDATE, you are agreeing to the Policy and Terms &amp; Conditions.</p></div>'+
							'						<div class="col-md-4"><button class="btn btn-primary pull-right" type="submit">Update <i class="fa fa-arrow-circle-right"></i></button></div>'+
							'					</div>'+
							'				</form>'+
							'			</div>'+
										
										
							'		</div>'+
							'	</div>'+
							'</div>';
				return str;
			}
			
			
			function coursesView(sem_no,theory,lab)
			{
						var str = "";
						var sem = ["zero","one","two","three","four","five","six","seven","eight"];
						
						str +=  
						'<div class="wrap-content container" id="container">' +
							
						
						
						
							'<div class="col-md-12">' +
								'<div class="panel panel-white no-radius">' +
									'<div class="panel-body">' +
										'<div class="partition-light-grey padding-15 text-center margin-bottom-20">' +
											'<h4 class="no-margin">Courses</h4>' +
											'<span class="text-light">for semester '+sem[sem_no]+'</span>' +
										'</div>' +
										'<div id="accordion" class="accordion accordion-white no-margin">' +
											'<div class="card no-radius">' +
											
												'<section id="page-title">' +
													//'<div class="row">' +
														'<div class="col-md-12">' +
															
															
																	'<span class="mainDescription">Select Semester</span>' +
																	'<select name="semester" id="semester_no" onchange="loadOnSemesterChange(this)" class="custom-select sources" placeholder="Source Type">' ;
																		for(var k=1;k<=8;k++)
																		str += '<option>'+k+'</option>';
													str +=			'</select>' +
																
														'</div>' +
													//'</div>' +
												'</section>' +
											
												'<div class="card-header" id="headingTwo">' +
													'<h4 class="card-title">' +
														'<a href="#collapseTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="accordion-toggle padding-15"><i class="icon-arrow"></i> Theory Courses <span class="badge badge-success">'+theory.length+'</span></a>' +
													'</h4>' +
												'</div>' +
												'<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">' +
													'<div class="card-body no-padding partition-light-grey">' +
														'<table class="table table-striped">' +
															'<thead><tr><th class="center">Course ID</th><th>Course name</th><th class="center">Credit</th><th></th></tr></thead>'+
															'<tbody>';
																for(val of theory)
																{
																	str += '<tr><td class="center">'+val.id+'</td><td>'+val.name+'</td><td class="center">'+val.credit+'</td><td><i class="fa fa-caret-down text-red"></i></td></tr>';
																}
															str += '</tbody>' +
														'</table>' +
													'</div>' +
												'</div>' +
											'</div>' +
											'<div class="card no-radius">' +
												'<div class="card-header" id="headingOne">' +
													'<h4 class="card-title">' +
														'<a href="#collapseOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="accordion-toggle padding-15"><i class="icon-arrow"></i> Lab Courses <span class="badge badge-success">'+lab.length+'</span></a>' +
													'</h4>' +
												'</div>' +
												'<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">' +
													'<div class="card-body no-padding partition-light-grey">' +
														'<table class="table table-striped">' +
															'<thead><tr><th class="center">Course ID</th><th>Course name</th><th class="center">Credit</th><th></th></tr></thead>'+
															'<tbody>' ;
																for(val of lab)
																{
																	str += '<tr><td class="center">'+val.id+'</td><td>'+val.name+'</td><td class="center">'+val.credit+'</td><td><i class="fa fa-caret-down text-red"></i></td></tr>';
																}
																
															str += '</tbody>' +
														'</table>' +
													'</div>' +
												'</div>' +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>'+
						'</div>' ;
					return str;

			}

			
			
			
			function loadCoursesContent(sem_no) 
			{
				
				var formData = {
					'semester_no'       :  sem_no
				};
				console.log(formData);

				$.ajax({
					url: "hitters/LoadCourses.php",
					type: "POST",
					data: formData,
					success: function(d) 
					{
						
						var obj = JSON.parse(d);
						var theory = obj["theory"];
						var lab = obj["lab"];
						
						var str = "";
						
						for(val of theory)
						{
							str += val.id;
						}
						
						
						
						$("#homeContent").empty().append(coursesView(sem_no,theory,lab));
						$("#semester_no").val(sem_no);
					}
				});
            }
			
			
			
			function loadOnSemesterChange(el) 
			{
				//alert("changed");
				loadCoursesContent($('#semester_no').val());
			}
			
			function loadOnSemesterChangeUpload(el) 
			{
				//alert("changed");
				//$('#semester_no_upload').val();
				
				
				
				//ajax
					
				var formData = 
				{
					'semester_no':  $('#semester_no_upload').val()
				};
				$.ajax(
				{
					url: "hitters/LoadCoursesID.php",
					type: "POST",
					data: formData,
					success: function(data) 
					{
						var obj = JSON.parse(data);
						console.log(obj);
						//alert("success");
						
						$('#course_id').empty();
						
						for(val of obj)
						{
							$('#course_id').append('<option class="custom-option" data-value="'+val.id+'">'+val.id+'</option>');
						}
					}
				});
				
			}
			
			function loadUploadContent()
			{
				var str="";
				
				str += '<div class="wrap-content container" id="container">' +
							
							'<section id="page-title">' +
								'<div class="row">' +
									'<div class="col-sm-8">' +
										'<h1 class="mainTitle">Marksheet File Upload</h1>' +
										'<span class="mainDescription"> Upload the marks of a particular subject of a particular semester on a particular year.</span>' +
										
										'<div class="row">' +
											'<div class="col-sm-8">' +
												'<span class="mainDescription">Select Semester</span>' +
												'<select name="semester" id="semester_no_upload" onchange="loadOnSemesterChangeUpload(this)" class="custom-select" placeholder="Slect Semester No">' ;
													for(var k=1;k<=8;k++)
													str += '<option class="custom-option" data-value="'+k+'">'+k+'</option>';
								str +=			'</select>' +
											'</div>' +
											'<div class="col-sm-8">' +
												'<span class="mainDescription">Select Course ID</span>' +
												'<select name="course_id" id="course_id"  class="custom-select sources" placeholder="Source Type">' +
												'</select>' +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</section>' +
							
							'<div class="container-fluid container-fullw bg-white">' +
								'<div class="row">' +
									'<div class="col-md-12">' +
										//'<div class="mb-3">' +
										//	'<p class="m-0 text-center"><a class="ml-2" href="http://www.dropzonejs.com/" target="_blank">dropzone</a></p>' +
										//'</div>' +
										
										//'<form class="dropzone mb-3 card d-flex flex-row justify-content-center flex-wrap" id="upload-widget" action="" method="post">' +
										//'</form>' +
										'<div id="upload-widget" class="dropzone  mb-3 card d-flex flex-row justify-content-center flex-wrap"></div>'+
										'<p><small class="text-muted">Only <span class="badge badge-success">XLSX</span> file is allowed</small></p>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' ;
				
				
				return str;
			}
			
			function parseDataFromXL(obj)
			{
				/* set up XMLHttpRequest */


				// replace it with your file path in local server
				var url = "http://localhost/PROJECT3/marksheet/"+obj.sheet_name;
				
				
				var oReq = new XMLHttpRequest();
				oReq.open("GET", url, true);
				oReq.responseType = "arraybuffer";

				oReq.onload = function(e) 
				{
					var arraybuffer = oReq.response;

					/* convert data to binary string */
					var data = new Uint8Array(arraybuffer);

					var arr = new Array();
					for (var i = 0; i != data.length; ++i) {
						arr[i] = String.fromCharCode(data[i]);
					}

					var bstr = arr.join("");

					var cfb = XLSX.read(bstr, { type: 'binary' });
					var dta=[];
					var i=0;

					cfb.SheetNames.forEach(function(sheetName, index) 
					{

						// Obtain The Current Row As CSV
						var fieldsObjs = XLS.utils.sheet_to_json(cfb.Sheets[sheetName]);
						
						
						fieldsObjs.map(function(field) 
						{
							//console.log(field);
							dta[i] = field;
							i++;
							
						});

					});
					
					
					console.log(dta);	
					//ajax
					
					var formData = 
					{
						'data'       :  dta,
						'semester_no':  obj.semester_no,
						'course_id'  :  obj.course_id
					};
					$.ajax(
					{
						url: "hitters/InsertMarks.php",
						type: "POST",
						data: formData,
						success: function() 
						{
							
							//console.log("success");
							alert("success");
						}
					});
					
					
				}
				
				oReq.send();
			}
			
			function resultView(sem_no,result)
			{
						var sum = 0.0;
						var creditSum = 0.0;
						for(val of result)
						{
							sum       += parseFloat(val.credit)*markToGPA(parseFloat(val.mark));
							creditSum += parseFloat(val.credit);
							
						}
						
						sum = sum/creditSum;
						
						var str = "";
						var sem = ["zero","one","two","three","four","five","six","seven","eight"];
						
						str +=  '<div class="wrap-content container" id="container">' +
							
						
						
							'<div class="col-md-12">' +
								'<div class="panel panel-white no-radius">' +
									'<div class="panel-body">' +
										'<div class="partition-light-grey padding-15 text-center margin-bottom-20">' +
											'<h4 class="no-margin">Result</h4>' +
											'<span class="text-light">for semester '+sem[sem_no]+'</span>' +
										'</div>' +
										'<div id="accordion" class="accordion accordion-white no-margin">' +
											'<div class="card no-radius">' +
												
												'<section id="page-title">' +
													'<div class="row">' +
														'<div class="col-md-12">' +
															
															
																	'<span class="mainDescription">Select Semester</span>' +
																	'<select class="custom-select" placeholder="Select Semester No" name="semester" id="semester_no_result" onchange="loadOnSemesterChangeResult(this)">' ;
																		for(var k=1;k<=8;k++)
																		str += '<option class="custom-option" value="'+k+'">'+k+'</option>';
													str +=			'</select>' +
																
														'</div>' +
													'</div>' +
												'</section>' +
												
												
												'<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">' +
													'<div class="card-body no-padding partition-light-grey">' +
														'<table class="table table-striped">' +
															
															'<thead><tr><th class="center">Course ID</th><th>Course name</th><th class="center">Marks</th><th class="center">Credit</th><th class="center">Grade</th></tr></thead>'+
															'<tbody>';
												for(val of result)
												{
													str += '<tr><td class="center">'+val.id+'</td><td>'+val.name+'</td><td class="center">'+val.mark+'</td><td class="center">'+val.credit+'</td><td class="center">'+markToGPA(parseFloat(val.mark))+'</td></tr>';
												}
															str += '</tbody>' +
															
															'<tfoot><tr><th></th><th></th><th></th><th>SGPA = </th><th>'+sum+'</th></tr></tfoot>'+
														'</table>' +
													'</div>' +
												'</div>' +
											'</div>' +
											
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'<div>';	
					return str;

			}

			
			function loadResultContent(reg_no,sem_no) 
			{
				
				var formData = {
					'register_no'       :  reg_no, 
					'semester_no'       :  sem_no
				};
				console.log(formData);

				$.ajax({
					url: "hitters/LoadResult.php",
					type: "POST",
					data: formData,
					success: function(d) 
					{
						var obj = JSON.parse(d);
						
						$("#homeContent").empty().append(resultView(sem_no,obj)); //
						//customSelectFunction();
						$("#semester_no_result").val(sem_no);
						
					}
				});
            }
			
			function loadOnSemesterChangeResult(el) 
			{
				//alert("changed");
				
				//customSelectFunction();
				loadResultContent('<?php echo $_SESSION['data']['reg_no']; ?>',$('#semester_no_result').val());
			}
			
			
			
			function customSelectFunction()
			{
				$(".-custom-select").each(function() {
					alert("entered");
				  var classes = $(this).attr("class"),
					  id      = $(this).attr("id"),
					  name    = $(this).attr("name");
				  var template =  '<div class="' + classes + '">';
					  template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
					  template += '<div class="custom-options">';
					  $(this).find("option").each(function() {
						template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' 
						+ $(this).html() + '</span>';
					  });
				  template += '</div></div>';
				  
				  $(this).wrap('<div class="custom-select-wrapper"></div>');
				  $(this).hide();
				  $(this).after(template);
				});
				$(".custom-option:first-of-type").hover(function() {
				  $(this).parents(".custom-options").addClass("option-hover");
				}, function() {
				  $(this).parents(".custom-options").removeClass("option-hover");
				});
				$(".custom-select-trigger").on("click", function() {
				  $('html').one('click',function() {
					$(".-custom-select").removeClass("opened");
				  });
				  $(this).parents(".-custom-select").toggleClass("opened");
				  event.stopPropagation();
				});
				$(".custom-option").on("click", function() 
				{
				  $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
				  $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
				  $(this).addClass("selection");
				  $(this).parents(".-custom-select").removeClass("opened");
				  $(this).parents(".-custom-select").find(".custom-select-trigger").text($(this).text());
				});
			}
		</script>
		<script>
			jQuery(document).ready(function()
			{
				$("#homeContent").empty().append(loadHomeContent());
				$("#nav_home").attr('class','active open');
				
				
				
				$("#nav_home").click(function() {
					
					$("#homeContent").empty().append(loadHomeContent());
					//document.getElementById("nav_home").className = "active open";
					$("#nav_home").attr('class','active open');
					
					$("#nav_courses").attr('class','');
					$("#nav_result").attr('class','');
					$("#nav_upload").attr('class','');
					$("#nav_profile").attr('class','');
					
				});
				
				
				$("#nav_profile").click(function() {
					
					$("#homeContent").empty().append(loadProfileContent());
					
					$("#nav_profile").attr('class','active open');
					
					$("#nav_home").attr('class','');
					$("#nav_courses").attr('class','');
					$("#nav_result").attr('class','');
					$("#nav_upload").attr('class','');
					
					
				});
				
				$("#nav_courses").click(function() {
					
					$("#homeContent").empty().append(loadCoursesContent(1));
					//document.getElementById("nav_home").className = "active open";
					
					$("#nav_courses").attr('class','active open');
					
					$("#nav_home").attr('class','');
					$("#nav_result").attr('class','');
					$("#nav_upload").attr('class','');
					$("#nav_profile").attr('class','');
					
				});
				
				$("#nav_result").click(function() {
					
					
					loadResultContent('<?php echo $_SESSION['data']['reg_no']; ?>',1);
					//customSelectFunction();
					
					$("#nav_result").attr('class','active open');
					
					$("#nav_courses").attr('class','');
					$("#nav_home").attr('class','');
					$("#nav_upload").attr('class','');
					$("#nav_profile").attr('class','');
					
				});
				
				
				$("#nav_upload").click(function() {
					
					$("#homeContent").empty().append(loadUploadContent());
					
					
					/* Dropzone's code */ //$("#upload-widget").dropzone({ url: "hitters/upload.php" });
					
					var markDropzone = new Dropzone("#upload-widget", 
					{ 
						url: "hitters/upload.php",
						maxFiles: 1,
						acceptedFiles: '.xlsx',
						paramName: 'file',
						init: function() 
						{
							var that = this;
							
							this.on("sending", function(file, xhr, formData)
							{
								if($('#semester_no_upload').val()<=0 || $('#course_id').val()<=0)
								{
									that.removeFile(file);
								}
								else
								{
									formData.append("semester_no", $('#semester_no_upload').val()),
									formData.append("course_id", $('#course_id').val())
								}
								
							}),
							
							this.on("success", function()
							{
								//alert(file.xhr.response);
								var args = Array.prototype.slice.call(arguments);
								var obj  = JSON.parse(args[1]);
								
								//console.log(obj.sheet_name);
								parseDataFromXL(obj);
								
								
							})
						}
					});
					
					
					
					
					
					/* Dropzone's code ends here*/
					
					$("#nav_upload").attr('class','active open');
					
					$("#nav_courses").attr('class','');
					$("#nav_home").attr('class','');
					$("#nav_result").attr('class','');
					$("#nav_profile").attr('class','');
					
				});
				
				
				
				
				
				
				Index.init();
				
				
			})
			
			
		</script>
	</body>
</html>
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--div class="col-md-12">
		<div class="tabbable">
			<ul class="nav nav-tabs" id="myTab4">
				<li class="nav-item padding-top-5 padding-left-5"><a href="#panel_overview" class="nav-link active" data-toggle="tab">Overview</a></li>
				<li class="nav-item padding-top-5"><a href="#panel_edit_account" class="nav-link" data-toggle="tab">Edit Account</a></li>
				<li class="nav-item padding-top-5"><a href="#panel_projects" class="nav-link" data-toggle="tab">Projects</a></li>
			</ul>
			<div class="tab-content">
				<div id="panel_overview" class="tab-pane active">
					<div class="row">
						
						<div class="col-sm-5 col-md-4"><div class="user-left"><div class="center"><h4>Peter Clark</h4><div class="fileinput fileinput-new" data-provides="fileinput"><div class="user-image"><div class="fileinput-new img-thumbnail"><img src="assets/images/avatar-1-xl.jpg" alt=""></div><div class="fileinput-preview fileinput-exists img-thumbnail"></div><div class="user-image-buttons"><span class="btn btn-azure btn-file btn-sm"><span class="fileinput-new"><i class="fa fa-pencil"></i> </span><span class="fileinput-exists"><i class="fa fa-pencil"></i> </span><input type="file"> </span><a href="#" class="btn fileinput-exists btn-red btn-sm" data-dismiss="fileinput"><i class="fa fa-times"></i></a></div></div></div><hr><div class="social-icons block"><ul><li data-placement="top" data-original-title="Twitter" class="social-twitter tooltips"><a href="http://www.twitter.com" target="_blank">Twitter</a></li><li data-placement="top" data-original-title="Facebook" class="social-facebook tooltips"><a href="http://facebook.com" target="_blank">Facebook</a></li><li data-placement="top" data-original-title="Google" class="social-google tooltips"><a href="http://google.com" target="_blank">Google+</a></li><li data-placement="top" data-original-title="LinkedIn" class="social-linkedin tooltips"><a href="http://linkedin.com" target="_blank">LinkedIn</a></li><li data-placement="top" data-original-title="Github" class="social-github tooltips"><a href="#" target="_blank">Github</a></li></ul></div><hr></div><table class="table table-condensed"><thead><tr><th colspan="3">Contact Information</th></tr></thead><tbody><tr><td>url</td><td><a href="#">www.example.com</a></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>email:</td><td><a href="">peter@example.com</a></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>phone:</td><td>(641)-734-4763</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>skye</td><td><a href="">peterclark82</a></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr></tbody></table><table class="table"><thead><tr><th colspan="3">General information</th></tr></thead><tbody><tr><td>Position</td><td>UI Designer</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Last Logged In</td><td>56 min</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Position</td><td>Senior Marketing Manager</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Supervisor</td><td><a href="#">Kenneth Ross</a></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Status</td><td><span class="badge badge-sm badge-info">Administrator</span></td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr></tbody></table><table class="table"><thead><tr><th colspan="3">Additional information</th></tr></thead><tbody><tr><td>Birth</td><td>21 October 1982</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr><tr><td>Groups</td><td>New company web site development, HR Management</td><td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td></tr></tbody></table></div></div>
						
						<div class="col-sm-7 col-md-8"><div class="row space20"><div class="col-sm-3"><button class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block"><i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i> Projects</button></div><div class="col-sm-3"><button class="btn btn-icon margin-bottom-5 btn-block"><i class="ti-comments block text-primary text-extra-large margin-bottom-10"></i> Messages <span class="badge badge-danger">23</span></button></div><div class="col-sm-3"><button class="btn btn-icon margin-bottom-5 btn-block"><i class="ti-calendar block text-primary text-extra-large margin-bottom-10"></i> Calendar</button></div><div class="col-sm-3"><button class="btn btn-icon margin-bottom-5 btn-block"><i class="ti-flag block text-primary text-extra-large margin-bottom-10"></i> Notifications</button></div></div><div class="panel panel-white" id="activities"><div class="panel-heading border-light"><h4 class="panel-title text-primary">Recent Activities</h4><paneltool class="panel-tools" tool-collapse="tool-collapse" tool-refresh="load1" tool-dismiss="tool-dismiss"></paneltool></div><div collapse="activities" ng-init="activities=false" class="panel-wrapper"><div class="panel-body"><ul class="timeline-xs"><li class="timeline-item success"><div class="margin-left-15"><div class="text-muted text-small">2 minutes ago</div><p><a class="text-info" href>Steven </a>has completed his account.</p></div></li><li class="timeline-item"><div class="margin-left-15"><div class="text-muted text-small">12:30</div><p>Staff Meeting</p></div></li><li class="timeline-item danger"><div class="margin-left-15"><div class="text-muted text-small">11:11</div><p>Completed new layout.</p></div></li><li class="timeline-item info"><div class="margin-left-15"><div class="text-muted text-small">Thu, 12 Jun</div><p>Contacted <a class="text-info" href>Microsoft </a>for license upgrades.</p></div></li><li class="timeline-item"><div class="margin-left-15"><div class="text-muted text-small">Tue, 10 Jun</div><p>Started development new site</p></div></li><li class="timeline-item"><div class="margin-left-15"><div class="text-muted text-small">Sun, 11 Apr</div><p>Lunch with <a class="text-info" href>Nicole </a>.</p></div></li><li class="timeline-item warning"><div class="margin-left-15"><div class="text-muted text-small">Wed, 25 Mar</div><p>server Maintenance.</p></div></li><li class="timeline-item"><div class="margin-left-15"><div class="text-muted text-small">Fri, 20 Mar</div><p>New User Registration <a class="text-info" href>more details </a>.</p></div></li></ul></div></div></div><div class="panel panel-white space20"><div class="panel-heading"><h4 class="panel-title">Recent Tweets</h4></div><div class="panel-body"><ul class="ltwt"><li class="ltwt_tweet"><p class="ltwt_tweet_text"><a href class="text-info">@Shakespeare </a>Some are born great, some achieve greatness, and some have greatness thrust upon them.</p><span class="block text-light"><i class="fa fa-fw fa-clock-o"></i> 2 minuts ago</span></li></ul></div></div></div>
					</div>
				</div>
				
				<div id="panel_edit_account" class="tab-pane">
					<form action="#" role="form" id="form">
						<fieldset><legend>Account Info</legend><div class="row"><div class="col-md-6"><div class="form-group"><badge class="control-badge">First Name</badge><input type="text" placeholder="Peter" class="form-control" id="firstname" name="firstname"></div><div class="form-group"><badge class="control-badge">Last Name</badge><input type="text" placeholder="Clark" class="form-control" id="lastname" name="lastname"></div><div class="form-group"><badge class="control-badge">Email Address</badge><input type="email" placeholder="peter@example.com" class="form-control" id="email" name="email"></div><div class="form-group"><badge class="control-badge">Phone</badge><input type="email" placeholder="(641)-734-4763" class="form-control" id="phone" name="email"></div><div class="form-group"><badge class="control-badge">Password</badge><input type="password" placeholder="password" class="form-control" name="password" id="password"></div><div class="form-group"><badge class="control-badge">Confirm Password</badge><input type="password" placeholder="password" class="form-control" id="password_again" name="password_again"></div></div><div class="col-md-6"><div class="form-group"><badge class="control-badge">Gender</badge><div class="clip-radio radio-primary"><input type="radio" value="female" name="gender" id="us-female"><badge for="us-female">Female</badge><input type="radio" value="male" name="gender" id="us-male" checked="checked"><badge for="us-male">Male</badge></div></div><div class="row"><div class="col-md-4"><div class="form-group"><badge class="control-badge">Zip Code</badge><input class="form-control" placeholder="12345" type="text" name="zipcode" id="zipcode"></div></div><div class="col-md-8"><div class="form-group"><badge class="control-badge">City</badge><input class="form-control tooltips" placeholder="London (UK)" type="text" data-original-title="We'll display it when you write reviews" data-rel="tooltip" title="" data-placement="top" name="city" id="city"></div></div></div><div class="form-group"><badge>Image Upload</badge><div class="fileinput fileinput-new mt-3" data-provides="fileinput"><div class="user-image"><div class="fileinput-new img-thumbnail"><img src="assets/images/avatar-1-xl.jpg" alt=""></div><div class="fileinput-preview fileinput-exists img-thumbnail"></div><div class="user-image-buttons"><span class="btn btn-azure btn-file btn-sm"><span class="fileinput-new"><i class="fa fa-pencil"></i> </span><span class="fileinput-exists"><i class="fa fa-pencil"></i> </span><input type="file"> </span><a href="#" class="btn fileinput-exists btn-red btn-sm" data-dismiss="fileinput"><i class="fa fa-times"></i></a></div></div></div></div></div></div></fieldset>
						<fieldset><legend>Additional Info</legend><div class="row"><div class="col-md-6"><div class="form-group"><badge class="control-badge">Twitter</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-twitter"></i></span></div><div class="form-group"><badge class="control-badge">Facebook</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-facebook"></i></span></div><div class="form-group"><badge class="control-badge">Google Plus</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-google-plus"></i></span></div></div><div class="col-md-6"><div class="form-group"><badge class="control-badge">Github</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-github"></i></span></div><div class="form-group"><badge class="control-badge">Linkedin</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-linkedin"></i></span></div><div class="form-group"><badge class="control-badge">Skype</badge><span class="input-icon"><input class="form-control" type="text" placeholder="Text Field"> <i class="fa fa-skype"></i></span></div></div></div></fieldset>
					
						<div class="row"><div class="col-md-12"><div>Required Fields<hr></div></div></div>
						
						<div class="row">
							<div class="col-md-8"><p>By clicking UPDATE, you are agreeing to the Policy and Terms &amp; Conditions.</p></div>
							<div class="col-md-4"><button class="btn btn-primary pull-right" type="submit">Update <i class="fa fa-arrow-circle-right"></i></button></div>
						</div>
					</form>
				</div>
				
				<div id="panel_projects" class="tab-pane"><table class="table" id="projects"><thead><tr><th>Project Name</th><th class="hidden-xs">Client</th><th>Proj Comp</th><th class="hidden-xs">%Comp</th><th class="hidden-xs center">Priority</th></tr></thead><tbody><tr><td>IT Help Desk</td><td class="hidden-xs">Master Company</td><td>11 november 2014</td><td class="hidden-xs"><div class="progress active progress-xs"><div style="width:70%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="70" role="progressbar" class="progress-bar progress-bar-warning"><span class="sr-only">70% Complete (danger)</span></div></div></td><td class="center hidden-xs"><span class="badge badge-danger">Critical</span></td></tr><tr><td>PM New Product Dev</td><td class="hidden-xs">Brand Company</td><td>12 june 2014</td><td class="hidden-xs"><div class="progress active progress-xs"><div style="width:40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-info"><span class="sr-only">40% Complete</span></div></div></td><td class="center hidden-xs"><span class="badge badge-warning">High</span></td></tr><tr><td>ClipTheme Web Site</td><td class="hidden-xs">Internal</td><td>11 november 2014</td><td class="hidden-xs"><div class="progress active progress-xs"><div style="width:90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar" class="progress-bar progress-bar-success"><span class="sr-only">90% Complete</span></div></div></td><td class="center hidden-xs"><span class="badge badge-success">Normal</span></td></tr><tr><td>Local Ad</td><td class="hidden-xs">UI Fab</td><td>15 april 2014</td><td class="hidden-xs"><div class="progress active progress-xs"><div style="width:50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-warning"><span class="sr-only">50% Complete</span></div></div></td><td class="center hidden-xs"><span class="badge badge-success">Normal</span></td></tr><tr><td>Design new theme</td><td class="hidden-xs">Internal</td><td>2 october 2014</td><td class="hidden-xs"><div class="progress active progress-xs"><div style="width:20%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-success"><span class="sr-only">20% Complete (warning)</span></div></div></td><td class="center hidden-xs"><span class="badge badge-danger">Critical</span></td></tr><tr><td>IT Help Desk</td><td class="hidden-xs">Designer TM</td><td>6 december 2014</td><td class="hidden-xs"><div class="progress active progress-xs"><div style="width:40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-warning"><span class="sr-only">40% Complete (warning)</span></div></div></td><td class="center hidden-xs"><span class="badge badge-warning">High</span></td></tr></tbody></table></div>
			</div>
		</div>
	</div-->			
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
				
			
			
			
			
			
			
			
			
			
			